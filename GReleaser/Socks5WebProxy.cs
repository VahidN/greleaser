using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace GReleaser
{
  class Socks5WebProxy : IWebProxy
  {
    public bool CheckValidationResult(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }
    public Socks5WebProxy(string _host, int _port)
    {
      host = _host;
      port = _port;
      
      ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(CheckValidationResult);
    }
    static Socket s = null;
    public Uri GetProxy(Uri destination)
    {
      if (s != null && s.Connected)
        return new Uri("http://" + host + ":" + port.ToString());

      string userName = "";
      string password = "";

      s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
      s.Connect(host, port);

      byte[] request = new byte[257];
      byte[] response = new byte[257];
      ushort nIndex = 0;

      request[nIndex++] = 0x05; // Version 5.
      request[nIndex++] = 0x02; // 2 Authentication methods are in packet...
      request[nIndex++] = 0x00; // NO AUTHENTICATION REQUIRED
      request[nIndex++] = 0x02; // USERNAME/PASSWORD
      // Send the authentication negotiation request...
      s.Send(request, nIndex, SocketFlags.None);
      // Receive 2 byte response...
      int nGot = s.Receive(response, 2, SocketFlags.None);
      if (nGot != 2) {
        Error = "Authentication negotiation request failed.";
        return null;
      }
      if (response[1] == 0xFF)
      {	// No authentication method was accepted close the socket.
        s.Close();
        Error = "None of the authentication method was accepted by proxy server.";
        return null;
      }

      byte[] rawBytes;

      nIndex = 0;
      request[nIndex++] = 0x05; // Version 5.

      // add user name
      request[nIndex++] = (byte)userName.Length;
      rawBytes = Encoding.Default.GetBytes(userName);
      rawBytes.CopyTo(request, nIndex);
      nIndex += (ushort)rawBytes.Length;

      // add password
      request[nIndex++] = (byte)password.Length;
      rawBytes = Encoding.Default.GetBytes(password);
      rawBytes.CopyTo(request, nIndex);
      nIndex += (ushort)rawBytes.Length;

      // Send the Username/Password request
      s.Send(request, nIndex, SocketFlags.None);
      // Receive 2 byte response...
      nGot = s.Receive(response, 2, SocketFlags.None);
      if (nGot != 2)
      {
        Error = "Bad response received from proxy server.";
        return null;
      }
      if (response[1] != 0x00)
      {
        Error = "Bad Usernaem/Password.";
        return null;
      }

      // Send connect request now...
      nIndex = 0;
      request[nIndex++] = 0x05;	// version 5.
      request[nIndex++] = 0x01;	// command = connect.
      request[nIndex++] = 0x00;	// Reserve = must be 0x00

      // Dest. address is domain name.
      IPAddress destIP = Dns.GetHostEntry(destination.Host).AddressList[0];
      {
        request[nIndex++] = 0x01;	// Address is full-qualified domain name.
        rawBytes = destIP.GetAddressBytes();
        rawBytes.CopyTo(request, nIndex);
        nIndex += (ushort)rawBytes.Length;
      }

      // using big-edian byte order
      ushort destPort = (ushort)destination.Port;
      byte[] portBytes = BitConverter.GetBytes(destPort);
      for (int i = portBytes.Length - 1; i >= 0; i--)
        request[nIndex++] = portBytes[i];

      // send connect request.
      s.Send(request, nIndex, SocketFlags.None);
      s.Receive(response);	// Get variable length response...
      if (response[1] != 0x00)
        Error = errorMsgs[response[1]];

      return new Uri("http://" + host + ":" + port.ToString());
    }

    public bool IsBypassed(Uri host)
    {
      return false;
    }

    public ICredentials Credentials
    { 
      get 
      {
        return null;
      }
      set
      {
      }
    }

    private string host;
    private int port;
    public string Error;

    private static string[] errorMsgs =	{
										"Operation completed successfully.",
										"General SOCKS server failure.",
										"Connection not allowed by ruleset.",
										"Network unreachable.",
										"Host unreachable.",
										"Connection refused.",
										"TTL expired.",
										"Command not supported.",
										"Address type not supported.",
										"Unknown error."
									};
  }
}
