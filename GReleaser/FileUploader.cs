using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace GReleaser
{
  class ProxyTcpClient : TcpClient
  {
    public void Connect(string target, int port, string proxyserver, int proxyport)
    {
      //System.Net.IPAddress ipTemp;
      //if (!System.Net.IPAddress.TryParse(proxyserver, out ipTemp))
      //  proxyserver = System.Net.Dns.GetHostEntry(proxyserver).AddressList[0].ToString();
      //if (!System.Net.IPAddress.TryParse(target, out ipTemp))
      //  target = System.Net.Dns.GetHostEntry(target).AddressList[0].ToString();
      Client = LMKR.SocksProxy.ConnectToSocks5Proxy(proxyserver, (ushort)proxyport, target, (ushort)port, "", "");
    }
    public Socket Socket
    {
      get
      {
        return Client;
      }
    }
  }
  class FileUploader
  {
    private static bool OnCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
      return errors == SslPolicyErrors.None;
    }
    public string Proxy
    {
      get { return proxy;  }
      set { proxy = value; }
    }
    public string Upload2(string file, string project, string user, string pass, string summary)
    {
      FileStream fileStream = File.OpenRead(file);
      BinaryReader reader = new BinaryReader(fileStream);
      int fileLength = Convert.ToInt32(fileStream.Length);
      byte[] fileContent = new byte[fileLength];
      reader.Read(fileContent, 0, fileLength);
      reader.Close();
      fileStream.Close();

      string host = project + ".googlecode.com";
      TcpClient tcp = null;
      if (proxy != null && proxy.Length > 0)
      {
        ProxyTcpClient proxyTcp = new ProxyTcpClient();
        string proxyServer = proxy.Substring(0, proxy.LastIndexOf(':'));
        int proxyPort = Convert.ToInt32(proxy.Substring(proxy.LastIndexOf(':') + 1));
        proxyTcp.Connect(host, 443, proxyServer, proxyPort);
        tcp = proxyTcp;
      }
      else
      {
        tcp = new TcpClient();
        tcp.Connect(host, 443);
      }
      RemoteCertificateValidationCallback callback = new RemoteCertificateValidationCallback(OnCertificateValidation);
      SslStream netStream = new SslStream(tcp.GetStream(), false, callback);
      netStream.AuthenticateAsClient(host);

      Dictionary<string, string> formFields = new Dictionary<string, string>();
      formFields.Add("summary", summary);
      string body = EncodeUploadRequest(formFields, file); //  content_type
      string endBoundary = crlf + "--" + boundary + "--" + crlf + crlf;

      int contentLength = (body.Length + fileLength + endBoundary.Length);
      string auth = System.Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(user + ":" + pass));
      string header = "POST /files HTTP/1.1" + crlf;
      header += "Host: " + host + crlf;
      header += "Accept-Encoding: identity" + crlf;
      header += "Content-Length: " + contentLength.ToString() + crlf;
      header += "Content-Type: multipart/form-data; boundary=" + boundary + crlf;
      header += "Authorization: Basic " + auth + crlf;
      header += "User-Agent: Googlecode.com uploader v0.9.4" + crlf;

      byte[] writeBuffer = new byte[contentLength + header.Length];
      ASCIIEncoding.ASCII.GetBytes(header).CopyTo(writeBuffer, 0);
      ASCIIEncoding.ASCII.GetBytes(body).CopyTo(writeBuffer, header.Length);
      fileContent.CopyTo(writeBuffer, header.Length + body.Length);
      ASCIIEncoding.ASCII.GetBytes(endBoundary).CopyTo(writeBuffer, header.Length + body.Length + fileLength);
      netStream.Write(writeBuffer);

      List<byte> readBuffer = new List<byte>();
      int i = -1;
      while ((i = netStream.ReadByte()) != -1)
        readBuffer.Add(Convert.ToByte(i));
      netStream.Close();
      tcp.Close();

      using (BinaryWriter writer2 = new BinaryWriter(new FileStream(@"c:\greleaser\dump", FileMode.Create))) { writer2.Write(writeBuffer); writer2.Close(); }
      
      if (readBuffer.Count == 0)
       return "Server didn't response.";
      return ASCIIEncoding.ASCII.GetString(readBuffer.ToArray());
    }
    public string Upload(string file, string project, string user, string pass, string summary)
    {
      if (user.Contains("@"))
        user = user.Substring(0, user.IndexOf('@'));

      Dictionary<string, string> formFields = new Dictionary<string, string>();
      formFields.Add("summary", summary);
      string body = EncodeUploadRequest(formFields, file); //  content_type
      string host = "https://" + project + ".googlecode.com/files";
      string auth = System.Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(user + ":" + pass));

      FileStream fileStream = File.OpenRead(file);
      BinaryReader reader = new BinaryReader(fileStream);
      byte[] fileContent = new byte[fileStream.Length];
      reader.Read(fileContent, 0, Convert.ToInt32(fileStream.Length));
      reader.Close();
      fileStream.Close();


      HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host);
      request.Headers.Add("Authorization", "Basic " + auth);
      request.UserAgent = "Googlecode.com uploader v0.9.4";// "GReleaser (code.google.com/p/greleaser)";
      request.ContentType = "multipart/form-data; boundary=" + boundary;
      request.Method = "POST";
      //request.Proxy = new Socks5WebProxy("tns2", 1082);
      //request.Expect = "";
    

      Stream requestStream = request.GetRequestStream();
      using (BinaryWriter requestContent = new BinaryWriter(requestStream))
      {
        requestContent.Write(ASCIIEncoding.ASCII.GetBytes(body));
        requestContent.Write(fileContent);
        requestContent.Write(ASCIIEncoding.ASCII.GetBytes(crlf + "--" + boundary + "--" + crlf));
        requestContent.Close();
      }

      //using (BinaryWriter writer2 = new BinaryWriter(new FileStream(@"c:\greleaser\dump", FileMode.Create))) { writer2.Write(ASCIIEncoding.ASCII.GetBytes(body)); writer2.Write(fileContent); writer2.Write(ASCIIEncoding.ASCII.GetBytes(crlf + "--" + boundary + "--" + crlf)); writer2.Close(); }

      string location = string.Empty;
      HttpWebResponse resp = null;
      try
      {
        resp = (HttpWebResponse)request.GetResponse();
        if (resp.StatusCode == HttpStatusCode.Created)
          location = resp.Headers["Location"];
      }
      catch
      {
      }
      return location;
    }

    public string Upload3(string file, string project, string user, string pass, string summary)
    {
      System.Diagnostics.Process process = new System.Diagnostics.Process();
      process.StartInfo.FileName = @"C:\Program Files\.dev\Python25\python";
      process.StartInfo.Arguments = @"googlecode-upload.py -s " + summary + " -p " + project + " -u " + user + " " + file;
      process.StartInfo.CreateNoWindow = true;
      process.StartInfo.RedirectStandardInput = true;
      process.StartInfo.RedirectStandardOutput = true;
      process.StartInfo.RedirectStandardError = true;
      process.StartInfo.UseShellExecute = false;
      process.Start();

      StreamWriter input = process.StandardInput;
      input.AutoFlush = true;
      input.WriteLine(pass);
      process.WaitForExit();
      string result = process.StandardOutput.ReadToEnd();
      return result;
    }

    private string EncodeUploadRequest(Dictionary<string, string> formFields, string file)
    {
      string body = crlf;
      foreach(KeyValuePair<string,string> it in formFields)
      {
        body += ("--" + boundary + crlf);
        body += ("Content-Disposition: form-data; name=\"" + it.Key + "\"" + crlf + crlf);
        body += (it.Value + crlf);
      }

      body += ("--" + boundary + crlf);
      body += ("Content-Disposition: form-data; name=\"filename\"; filename=\"" + Path.GetFileName(file) + "\"" + crlf);
      body += ("Content-Type: application/octet-stream" + crlf + crlf);

      return body;
    }

    string boundary = "----------Googlecode_boundary_reindeer_flotilla";
    string crlf = "\r\n";
    string proxy;
  }

  // ------------Googlecode_boundary_reindeer_flotilla
  // Content-Disposition: form-data; name="summary"
  // test
  // ------------Googlecode_boundary_reindeer_flotilla
  // Content-Disposition: form-data; name="filename"; filename="GReleaser.exe"
  // Content-Type: application/octet-stream
  // ------------Googlecode_boundary_reindeer_flotilla--
  // Content-TypeAuthorizationUser-Agent
}
