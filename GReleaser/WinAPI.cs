using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace GReleaser
{
  class WinAPI
  {
    [StructLayout(LayoutKind.Sequential)]
    private struct PROCESSENTRY32
    {
      public uint dwSize;
      public uint cntUsage;
      public uint th32ProcessID;
      public IntPtr th32DefaultHeapID;
      public uint th32ModuleID;
      public uint cntThreads;
      public uint th32ParentProcessID;
      public int pcPriClassBase;
      public uint dwFlags;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public string szExeFile;
    };

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern IntPtr CreateToolhelp32Snapshot(SnapshotFlags dwFlags, uint th32ProcessID);

    [DllImport("kernel32.dll")]
    private static extern bool Process32First(IntPtr hSnapshot, ref PROCESSENTRY32 lppe);

    [DllImport("kernel32.dll")]
    private static extern bool Process32Next(IntPtr hSnapshot, ref PROCESSENTRY32 lppe);

    [Flags]
    private enum SnapshotFlags : uint
    {
      HeapList = 0x00000001,
      Process = 0x00000002,
      Thread = 0x00000004,
      Module = 0x00000008,
      Module32 = 0x00000010,
      Inherit = 0x80000000,
      All = 0x0000001F
    }

    static public string GetParentProcessName()
    {
      int thisPid = System.Diagnostics.Process.GetCurrentProcess().Id;
      IntPtr handle = WinAPI.CreateToolhelp32Snapshot(WinAPI.SnapshotFlags.All, Convert.ToUInt32(thisPid));
      WinAPI.PROCESSENTRY32 info = new WinAPI.PROCESSENTRY32();
      info.dwSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(WinAPI.PROCESSENTRY32));
      while (WinAPI.Process32Next(handle, ref info))
        if (thisPid == info.th32ProcessID)
        {
          System.Diagnostics.Process parent = System.Diagnostics.Process.GetProcessById(Convert.ToInt32(info.th32ParentProcessID));
          return parent.MainModule.ModuleName;
        }
      return "";
    }
  }
}
