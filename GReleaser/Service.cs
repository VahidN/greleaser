using System.ServiceProcess;
using System.Configuration.Install;

namespace GReleaser
{
  class Service
  {
    static public string Name
    {
      get { return "GReleaser"; }
    }

    public static bool Install()
    {
      try
      {
        ManagedInstallerClass.InstallHelper(new string[] { System.Windows.Forms.Application.ExecutablePath });
        return true;
      }
      catch
      {
        return false;
      }
    }
    public static bool Uninstall()
    {
      try
      {
        ManagedInstallerClass.InstallHelper(new string[] { "/u", System.Windows.Forms.Application.ExecutablePath });
        return true;
      }
      catch
      {
        return false;
      }
    }
    public static void Start()
    {
      if ( IsInstalled(GReleaser.Service.Name) )
      {
        ServiceController s = new ServiceController(GReleaser.Service.Name);
        s.Start();
      }
    }
    public static void Stop()
    {
      if ( IsInstalled(GReleaser.Service.Name) )
      {
        ServiceController s = new ServiceController(GReleaser.Service.Name);
        s.Stop();
      }
    }
    public static void Run()
    {
      System.ServiceProcess.ServiceBase.Run(new GReleaserService());
    }
    public static bool IsInstalled(string service)
    {
      try
      {
        ServiceController s = new ServiceController(service);
        ServiceControllerStatus status = s.Status;
        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
