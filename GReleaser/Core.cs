using System;
using System.Collections.Generic;
using System.Text;

namespace GReleaser
{
  public class Core : IDisposable
  {
    public Core()
    {
      network = new Network(this);
      script = new Script(this);
      jobsQueue = new JobsQueue(this);
    }

    public void Dispose()
    {
      network.Dispose();
    }

    public Network Network
    {
      get 
      { 
        return network; 
      }
    }
    Network network = null;
    public Script script = null;
    public JobsQueue jobsQueue = null;
  }
}
