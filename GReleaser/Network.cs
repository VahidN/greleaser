using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace GReleaser
{
  public class Network : IDisposable
  {
    public Network(Core _core)
    {
      IPAddress host = Dns.GetHostEntry(Environment.MachineName).AddressList[0];
      net = new TcpListener(host, 8871);
      core = _core;
      net.Start();
      thinkThread.Start(this);
    }
    public void Dispose()
    {
      thinkThread.Abort();
      net.Stop();
    }
    static void Think(object param)
    {
      while(true)
      {
        Network network = (Network)param;
        if (!network.net.Pending())
        {
          foreach (Guid guid in network.receivedCommandsToRemove)
            network.receivedCommands.Remove(guid);
          foreach (Guid guid in network.sentCommandsToRemove)
            network.sentCommands.Remove(guid);
          network.sentCommandsToRemove.Clear();
          network.receivedCommandsToRemove.Clear();

          foreach (KeyValuePair<Guid, Command> it in network.sentCommands)
            if (it.Value.Source.Connected && it.Value.Source.Available > 0)
              network.ProcessStream(it.Value.Source.GetStream(), null);
          foreach (KeyValuePair<Guid, Command> it in network.receivedCommands)
            if (it.Value.Source.Connected && it.Value.Source.Available > 0)
              network.ProcessStream(it.Value.Source.GetStream(), null);
        }
        else
        {
          TcpClient connection = network.net.AcceptTcpClient();
          NetworkStream stream = connection.GetStream();
          network.ProcessStream(stream, connection);
        }
        Thread.Sleep(1);
      }
    }
    void ProcessStream(NetworkStream stream, TcpClient connection)
    {
      StreamReader reader = new StreamReader(stream);
      {
        string firstLine = reader.ReadLine();
        int dataLength = int.Parse(reader.ReadLine());
        char[] rawData = new char[dataLength];
        reader.Read(rawData, 0, dataLength);
        string data = new string(rawData);

        if (firstLine.StartsWith("COMMAND"))
        {
          XmlDocument xmlDoc = new XmlDocument();
          xmlDoc.LoadXml(data);
          string root = xmlDoc.DocumentElement.Name;
          Type type = Type.GetType("GReleaser.Commands." + root);
          XmlSerializer serializer = new XmlSerializer(type);// typeof(Command));
          Command command = serializer.Deserialize(new StringReader(data)) as Command;
          if (command != null)
          {
            command.Source = connection;
            command.Core = core;
            command.Execute();
            receivedCommands.Add(command.Guid, command);
          }
        }
        else if (firstLine.StartsWith("RESPONSE"))
        {
          Guid guid = new Guid(firstLine.Substring(firstLine.IndexOf(' ') + 1));
          responses[guid] = data;
        }
        else if (firstLine.StartsWith("DISPOSE"))
        {
          Guid guid = new Guid(firstLine.Substring(firstLine.IndexOf(' ') + 1));
          if (receivedCommands.ContainsKey(guid))
            receivedCommandsToRemove.Add(guid);
          if (responses.ContainsKey(guid))
            responses.Remove(guid);
        }
      }
    }
    public void Send(Command cmd)
    {
      sentCommands.Add(cmd.Guid, cmd);
      XmlSerializer serializer = new XmlSerializer(cmd.GetType());
      StringWriter stringStream = new StringWriter();
      StreamWriter writer = new StreamWriter(cmd.Source.GetStream());
      writer.AutoFlush = true;
      {
        writer.WriteLine("COMMAND");
        serializer.Serialize(stringStream, cmd);
        string xml = stringStream.ToString();
        writer.WriteLine(xml.Length);
        writer.WriteLine(xml);
      }
      cmd.Core = core;
    }

    public void SendResponse(Command cmd, string response)
    {
      StreamWriter writer = new StreamWriter(cmd.Source.GetStream());
      writer.AutoFlush = true;
      writer.WriteLine("RESPONSE" + " " + cmd.Guid.ToString());
      writer.WriteLine(response.Length);
      writer.WriteLine(response);
    }

    public string GetResponse(Command cmd)
    {
      string s = string.Empty;
      if (responses.ContainsKey(cmd.Guid))
        s = responses[cmd.Guid];
      return s;
    }

    public void DisposeCommand(Command cmd)
    {
      if (cmd.Source.Connected)
      {
        StreamWriter writer = new StreamWriter(cmd.Source.GetStream());
        writer.AutoFlush = true;
        writer.WriteLine("DISPOSE" + " " + cmd.Guid.ToString());
        writer.WriteLine("0");
      }
      if (sentCommands.ContainsKey(cmd.Guid))
        sentCommandsToRemove.Add(cmd.Guid);
    }

    private TcpListener net = null;
    private Thread thinkThread = new Thread(new ParameterizedThreadStart(Think));
    private Core core = null;
    private Dictionary<Guid, Command> sentCommands = new Dictionary<Guid, Command>();
    private Dictionary<Guid, Command> receivedCommands = new Dictionary<Guid, Command>();
    private Dictionary<Guid, string> responses = new Dictionary<Guid, string>();
    private List<Guid> sentCommandsToRemove = new List<Guid>(); // Dictionary not synchronized...
    private List<Guid> receivedCommandsToRemove = new List<Guid>(); // Dictionary not synchronized...
  }
}
