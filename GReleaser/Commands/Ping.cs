using System;
using System.Collections.Generic;
using System.Text;

namespace GReleaser.Commands
{
  public class Ping : Command
  {
    protected override void OnExecute()
    {
      SendResponse("PONG");
    }
  }
}
