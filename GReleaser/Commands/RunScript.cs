using System;
using System.Collections.Generic;
using System.Text;

namespace GReleaser.Commands
{
  public class RunScript : Command
  {
    public RunScript(string _script)
    {
      script = _script;
    }

    protected override void OnExecute()
    {
      Core.script.Run(script, null);
    }

    public string Script
    {
      get
      {
        return script;
      }
    }
    string script;
  }
}
