using System;
using System.Collections.Generic;
using System.Text;

namespace SvnExe
{
  public class Info
  {
    public Info()
    {
    }

    public string LastModAuthor;
    public int Revision;
  }
  public class Svn
  {
    public Svn(string url)
    {
      this.url = url;
    }

    public enum Revision
    {
      All = 0,
      Head = -1,
      Base = -2,
      Prev = -3,
    }

    public string[] Log(Revision revision)
    {
      return Log(Convert.ToInt32(revision));
    }
    public string[] Log(int revision)
    {
      string rev;
      switch ((Revision)revision)
      {
        case Revision.All: rev = ""; break;
        case Revision.Head: rev = " -r Head"; break;
        case Revision.Base: rev = " -r Base"; break;
        case Revision.Prev: rev = " -r Prev"; break;
        default: rev = " -r " + revision.ToString(); break;
      }
      string result = ExecSvn("log", url + rev);
      string[] lines = result.Split(new char[] { '\n' });
      List<string> log = new List<string>();
      bool skipNext = false;
      foreach (string line in lines)
      {
        if (skipNext)
        {
          skipNext = false;
          continue;
        }
        else if (line.Length == 73 && line.TrimStart(new char[] { '-', '\r', '\n' }).Length == 0)
          skipNext = true;
        else if (line.Trim().Length > 0 )
          log.Add(line.Trim());
      }
      return log.ToArray();
    }
    
    public SvnExe.Info Info
    {
      get
      {
        string result = ExecSvn("info", url);
        string[] lines = result.Split(new char[] { '\n' });
        SvnExe.Info info = new SvnExe.Info();
        foreach (string line in lines)
        {
          if (line.StartsWith("Revision: "))
            info.Revision = Convert.ToInt32(line.Substring(10));
          if (line.StartsWith("Last Changed Author: "))
            info.LastModAuthor = line.Substring(line.IndexOf(':') + 1);
        }
        return info;
      }
    }

    private string ExecSvn(string command, string param)
    {
      System.Diagnostics.Process process = new System.Diagnostics.Process();
      process.StartInfo.FileName = "svn";
      process.StartInfo.Arguments = command + " " + param;
      process.StartInfo.CreateNoWindow = true;
      process.StartInfo.RedirectStandardOutput = true;
      process.StartInfo.UseShellExecute = false;
      process.Start();
      string result = process.StandardOutput.ReadToEnd();
      process.WaitForExit();
      return result;
    }

    private string url;
  }
}
