using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace GReleaser
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(string[] args)
    {
      string parentName = WinAPI.GetParentProcessName();
      if ( parentName.IndexOf("services", StringComparison.OrdinalIgnoreCase) != -1 )
      {
        Service.Run();
        return;
      }

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new Form1());
    }
  }
}