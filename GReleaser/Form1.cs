using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;

namespace GReleaser
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
      richTextBox1.Text = Environment.MachineName;// "greleaser.googlecode.com";
      button1.Focus();
    }
    ~Form1()
    {
      core.Dispose();  
    }

    private void button1_Click(object sender, EventArgs e)
    {
      try
      {
        richTextBox2.Text = "PING\n";
        Command cmd = new Commands.Ping();
        cmd.Source = new System.Net.Sockets.TcpClient(richTextBox1.Text, 8871);
        cmd.Core = core;
        core.Network.Send(cmd);
        cmd.WaitResponse();
        richTextBox2.Text += cmd.Response;
        cmd.Dispose();
      }
      catch
      {
        richTextBox2.Text += "Cannot connect!";
      }
/*
      return;


      button1.Enabled = false;
      richTextBox2.Clear();
      string s2 = string.Empty;
      FileUploader f = new FileUploader();
      //f.Proxy = "tns2:1082";
      //s2 = f.Upload3(@"C:\GReleaser\GReleaser\bin\Release\GReleaser_1_0_0_5.exe", "greleaser", "marinov.ivan.miklos", "", "test");
      //Service.Uninstall();
      //Service.Install();

      s2 = ((System.Net.HttpStatusCode)int.Parse(s2)).ToString();
      richTextBox2.Text = System.IO.Path.GetTempFileName() + "\n" + s2;
      button1.Enabled = true;
      //remote.LogEvent += new Client.LogEventHandler(delegate(string s) { richTextBox2.Text = s;  });
      //button1.Enabled = false;
      //Script.Run(richTextBox1.Text, remote);
      //button1.Enabled = true;
 */
    }

    Client remote = new Client();

    private void button2_Click(object sender, EventArgs e)
    {
      Service.Install();
      richTextBox2.Text = "installed:" + Service.IsInstalled("GReleaser").ToString();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      Service.Uninstall();
      richTextBox2.Text = "installed:" + Service.IsInstalled("GReleaser").ToString();
    }

    private void button4_Click(object sender, EventArgs e)
    {
      Service.Start();
    }

    private void button5_Click(object sender, EventArgs e)
    {
      Service.Stop();
      core.Network.Send(new Commands.RunScript(""));
    }

    Core core = new Core();
  }
}