using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.CodeDom.Compiler;

namespace GReleaser
{
  public class Script
  {
    public Script(Core _core)
    {
      core = _core;
    }
    public bool IsRunning()
    {
      return true;
    }
    public void Run(string source, Client client)
    {
      CodeDomProvider compiler = CodeDomProvider.CreateProvider("CSharp");
      CompilerParameters cp = new CompilerParameters();
      cp.GenerateExecutable = false;
      cp.GenerateInMemory = true;
      cp.ReferencedAssemblies.Add("system.dll");
      cp.ReferencedAssemblies.Add("greleaser.exe");
      //cp.ReferencedAssemblies.Add("system.windows.forms.dll");
      string src = "using SvnExe; " +
       "class ScriptRunner" +
       "{" +
       "public static void Run(GReleaser.Client Client, object[] param)" +
       "{" +
       source +
       "}" +
       "}";
      CompilerResults results = compiler.CompileAssemblyFromSource(cp, src);
      //foreach (System.CodeDom.Compiler.CompilerError ce in cr.Errors)
      // MessageBox.Show(ce.ErrorText); 

      if (results.Errors.HasErrors)
      {
        //if (results.Errors[0].ErrorNumber == "CS0029")
        //  return StringEval("Invoke(delegate { " + expr + "; })");
        //return results.Errors[0].ErrorText;
        client.Write("COMPILER ERROR:\n" + results.Errors[0].ErrorText);
      }
      else
      {
        Assembly assm = results.CompiledAssembly;
        Type target = assm.GetType("ScriptRunner");
        MethodInfo method = target.GetMethod("Run");
        method.Invoke(null, new object[] { client, null });
      }
    }
    private Core core;
  }
}
