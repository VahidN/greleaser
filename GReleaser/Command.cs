using System;
using System.Net;
using System.Net.Sockets;

namespace GReleaser
{
  public class Command : IDisposable
  {
    public void Dispose()
    {
      Core.Network.DisposeCommand(this);
    }

    public void Execute()
    {
      OnExecute();
    }

    public void WaitResponse()
    {
      WaitResponse(2000);
    }
    public void WaitResponse(int timeOut)
    {
      DateTime time = DateTime.Now;
      while ((DateTime.Now - time) < TimeSpan.FromMilliseconds(timeOut))
        if (Response.Length > 0)
          return;
    }

    protected virtual void OnExecute()
    {
    }

    protected void SendResponse(string s)
    {
      Core.Network.SendResponse(this, s);
    }

    [System.Xml.Serialization.XmlIgnore]
    public TcpClient Source
    {
      get
      {
        return source;
      }
      set
      {
        source = value;
      }
    }

    [System.Xml.Serialization.XmlIgnore]
    public string Response
    {
      get
      {
        return Core.Network.GetResponse(this);
      }
    }

    [System.Xml.Serialization.XmlIgnore]
    public Core Core
    {
      get
      {
        return core;
      }
      set
      {
        core = value;
      }
    }

    public System.Guid Guid
    {
      get
      {
        return guid;
      }
      set
      {
        guid = value;
      }
    }

    TcpClient source;
    System.Guid guid = System.Guid.NewGuid();
    Core core;
  }
}
