using System;
using System.Collections.Generic;
using System.Text;

namespace GReleaser
{
  public class Client
  {
    public void Write(string[] lines)
    {
      string s = string.Empty;
      foreach(string line in lines)
        s += line + "\n";
      LogEvent.Invoke(s);
    }
    public void Write(string s)
    {
      LogEvent.Invoke(s);
    }

    public delegate void LogEventHandler(string s);
    public event LogEventHandler LogEvent;
  }
}
