using System.Configuration.Install;
using System.ServiceProcess;
using System.ComponentModel;

namespace GReleaser
{
  public class GReleaserService : System.ServiceProcess.ServiceBase
  {
    public GReleaserService()
    {
      ServiceName = GReleaser.Service.Name;
      CanStop = true;
    }

    protected override void OnStart(string[] args)
    {
      core = new Core();
      //_manager.Start(new CManager.VoidVoidDelegate(OnUpdateJobs), new CManager.VoidVoidDelegate(OnUpdateUsers), EventLog);
      base.OnStart(args);
    }

    protected override void OnStop()
    {
      //if (_manager != null)
      //{
      //  _manager.Stop();
      //  _manager = null;
      //}
      base.OnStop();
    }
    protected override void OnContinue()
    {
      base.OnContinue();
    }
    private Core core;
  }

  [RunInstaller(true)]
  public class ProjectInstaller : Installer
  {
    public ProjectInstaller()
    {
      ServiceProcessInstaller processInstaller = new ServiceProcessInstaller();
      ServiceInstaller serviceInstaller = new ServiceInstaller();
      processInstaller.Account = ServiceAccount.LocalSystem;
      serviceInstaller.StartType = ServiceStartMode.Automatic;
      serviceInstaller.ServiceName = GReleaser.Service.Name;
      serviceInstaller.Description = "Provides network connection for GReleaser clients to be able to send and run job scripts.";
      Installers.Add(serviceInstaller);
      Installers.Add(processInstaller);
    }
  }
}
